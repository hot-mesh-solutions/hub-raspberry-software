#!/usr/bin/env python3

import logging as log
import socket
import threading
import time

import serial
from influxdb import InfluxDBClient

import random
import struct
import copy

import math

_data_types = { #length
    1: 1, #battery
    2: 1, #solar
    3: 4  #data
}


class HMSHub:
    tcp_port = 12345
    tcp_host = 'localhost'
    tcp_buffer_size = 1024
    influx_host = 'localhost'
    influx_port = 8086
    serial_port = '/dev/serial0'
    serial_baud = 115200
    uart_command_header = '!!!!'
    handshake_code = '18A835'

    def __init__(self):
        self.threads = [
            threading.Thread(target=self.handle_incoming_serial)
        ]

        self.ser = serial.Serial(
            port=self.serial_port,
            baudrate=self.serial_baud,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            #timeout=1
        )

        self.idbclient = InfluxDBClient(host=self.influx_host, port=self.influx_port)

        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # socket to accept connections from peers
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # rebind if possible

        self.lock = threading.Lock()

    def start(self):
        while True:
            try:
                self.idbclient.switch_database('hms_data')
                log.info('influxdb-client setup')
                break
            except Exception as e:
                log.error('influxdb-client failed to setup: %s' % str(e))
            time.sleep(5)

        for t in self.threads:
            t.start()  # start threads

        while True:  # bind to the port
            try:
                #self.server.bind((self.tcp_host, self.tcp_port))
                self.server.bind(('', self.tcp_port))
                break
            except socket.error:
                log.error('Bind failed %s' % str(socket.error))
            time.sleep(5)

        print("start listening")
        self.server.listen(10)  # listen for connections
        print("stop listening")

        log.info('Listening for handshake')

        while True:
            conn, addr = self.server.accept()

            print(conn, addr)

            print (conn, addr)
            conn.settimeout(60.0)
            socket_data = ''
            while True:
                socket_part = conn.recv(self.tcp_buffer_size).decode()
                socket_data = socket_data + socket_part
                if len(socket_part) < self.tcp_buffer_size:
                    break

            if socket_data == self.handshake_code:
                log.info('Handshake successful')
                conn.sendall(self.handshake_code.encode())
                conn.close()
                break
            else:
                log.warning('handshake failed. Got: %s' % str(socket_data))
            conn.close()

        log.info('listening for network connections')

        while True:
            conn, addr = self.server.accept()
            print("incoming from", addr)
            log.info('Incoming socket connection from %s' % str(addr))
            threading.Thread(target=self.handle_socket, args=(conn,addr)).start()
            print("hello?")

    def handle_incoming_serial(self):

        log.info('serial monitor started')

        _data_base = {
                "measurement": "n/a",
                "tags": {
                    "host": "n/a"
                },
                "time": "1",
                "fields": {
                    "value": 0.0
                }
            } #base data body

        # 3412010000000901F102BF03DEADBEEF
        #
        # send_sn: 0x1234 (|3412|010000000901F102BF03DEADBEEF)
        # timestamp: 1 second after 1970 (3412|01000000|0901F102BF03DEADBEEF)
        # num_data_bytes: 9 bytes 341201000000|09|01F102BF03DEADBEEF
        # Sensors:
        # Battery voltage @ 0xF1 (34120100000009|01|F1|02BF03DEADBEEF)
        # Solar voltage @ 0xBF (3412010000000901F1|02|BF|03DEADBEEF)
        # Temperature @ 1E-67 (3412010000000901F102BF|03|DEADBEEF|)

        # _data_types = {
        #     1: "battery_voltage",
        #     2: "solar_voltage",
        #     3: "temperature"
        # }

        while True:
            data_in = self.ser.readline()

            print(time.time(), "----------------------------------------------------")

            print("data_in (BYT):",data_in)
            print("data_in (HEX):",data_in.hex())
            print("data_in (DEC):",list(data_in))

            #print(data_in[4], self.uart_command_header.encode() in data_in)

            #log.debug('data_in: %s' % str(data_in))

            #print(list(data_in));

            if self.uart_command_header.encode() in data_in: # incoming command
                command = data_in[4]
                data_in = data_in[5:]

                #print (command)

                #print(command == "D".encode())

                if command == "D".encode()[0]:
                    #print(int.from_bytes(data_in[0:2], 'little'))
                    msg_sender = int.from_bytes(data_in[0:2], 'little') #2bytes

                    msg_time = None

                    try:
                        time_test = int.from_bytes(data_in[2:6], 'little')
                        msg_time = time.gmtime(time_test) #4bytes
                    except:
                        print('time err')
                        msg_time = time.gmtime(int(time.time()))

                    #print(msg_time)

                    data_length_bytes = 0

                    try:
                        data_length_bytes = data_in[6] #1byte
                    except:
                        print("error: msg not expected length")

                    #print(data_length_bytes)

                    battery_voltage = None
                    solar_voltage = None
                    data_value = None

                    data_index = 0

                    #print(data_in)
                    #print(list(data_in))

                    try:
                        while (1):

                            if (data_index >= data_length_bytes):
                                break

                            data_type = data_in[7+data_index]

                            if data_type not in [1, 2, 3]:
                                continue

                            data_point_length = _data_types[data_type]

                            if (data_type == 1):
                                battery_voltage = data_in[7+data_index+1]
                                data_index += 2
                            elif (data_type == 2):
                                solar_voltage = data_in[7+data_index+1]
                                data_index += 2
                            elif (data_type == 3):
                                data_value = struct.unpack('<f', bytes(data_in[7+data_index+1:7+data_index+5]))[0]
                                data_index += 5
                    except:
                        continue

                    try:
                        log.debug('Decoded MSG @{}: {} {} {} {} {} {}'.format(int(time.time()), msg_sender, int.from_bytes(data_in[2:6], 'little'), data_length_bytes, 5*(battery_voltage/255), 12*(solar_voltage/255), data_value))
                        log.debug('TIME ERROR: {}'.format(int.from_bytes(data_in[2:6], 'little') - int(time.time())))
                    except:
                        print("error 3.2")
                        continue

                    data_body = []

                    if not battery_voltage == None and (0 < 5.0*(float(battery_voltage)/255.0) < 5):
                        data_point = _data_base.copy()
                        data_point["measurement"] = "battery_voltage"
                        data_point["tags"]["host"] = str(msg_sender)
                        data_point["time"] = time.strftime("20%y-%m-%dT%H:%M:%S.0Z", msg_time)
                        data_point["fields"]["value"] = 5.0*(float(battery_voltage)/255.0)
                        data_body+=[copy.deepcopy(data_point)]
                    else:
                        print("info: invalid battery voltage", 5.0*(float(battery_voltage)/255.0))

                    if not solar_voltage == None and (0 < 12.0*(float(solar_voltage)/255.0) < 12.0):
                        data_point = _data_base.copy()
                        data_point["measurement"] = "solar_voltage"
                        data_point["tags"]["host"] = str(msg_sender)
                        data_point["time"] = time.strftime("20%y-%m-%dT%H:%M:%S.0Z", msg_time)
                        data_point["fields"]["value"] = 12.0*(float(solar_voltage)/255.0)
                        data_body+=[copy.deepcopy(data_point)]
                    else:
                        print("info: invalid solar voltage", 12.0*(float(solar_voltage)/255.0))

                    if not data_value == None and not math.isnan(data_value) and (0 < data_value < 40):
                        data_point = _data_base.copy()
                        data_point["measurement"] = "temperature"
                        data_point["tags"]["host"] = str(msg_sender)
                        data_point["time"] = time.strftime("20%y-%m-%dT%H:%M:%S.0Z", msg_time)
                        data_point["fields"]["value"] = data_value
                        data_body+=[copy.deepcopy(data_point)]
                    else:
                        print("info: invalid sensor value", data_value)

                    #print(data_body)

                    try:
                        self.idbclient.write_points(data_body)
                    except Exception as e:
                        log.error('Influx error: {}'.format(str(e)))


                elif command == "T".encode()[0]: #time
                    #send_time_bytes = int(time.time()).to_bytes(4, byteorder='little')
                    #bytes_written = self.ser.write(send_time_bytes)
                    self.ser.flush()
                    self.send_serial(int(time.time()), 4)
                    #self.ser.flush()
                    #log.debug('Sending time: {} {} {}'.format(bytes_written, int.from_bytes(send_time_bytes, 'little'), send_time_bytes))
                    #self.send_serial(send_time.to_bytes(4, byteorder='little'), 4)

                # elif command == "p": #power
                #     self.send_serial([int(0), int(0)], [2, 1])
                #
                # elif command == "s": #sample-rate
                #     self.send_serial([int(0), int(0)], [2, 1])

    def handle_socket(self, conn, addr):

        conn.settimeout(60.0)

        socket_data = ''

        try:
            while True:
                socket_part = conn.recv(self.tcp_buffer_size).decode()
                socket_data = socket_data + socket_part
                if len(socket_part) < self.tcp_buffer_size:
                    break
        except:
            print("ech")
            conn.close()
            return

        if socket_data == self.handshake_code:
            conn.sendall(self.handshake_code.encode())
            conn.close()
            return

        #conn.sendall(bytes([148,148]))
        response = bytes([4, 1, 50]) + int(time.time()).to_bytes(4, 'big') + int(random.randint(20, 21)).to_bytes(2, 'big')
        conn.sendall(response)

        conn.close()

        print("end")



    def send_serial(self, msg, size=1):

        if type(msg) is not list:
            msg = [msg]
        if type(size) is not list:
            size = [size]

        print(msg, size)

        byte_msg = []

        for i, msg_part in enumerate(msg):

            print(i, msg_part)

            if type(msg_part) is int:
                #print("int")
                byte_msg += msg_part.to_bytes(size[i], byteorder='little')

            elif type(msg_part) is str:
                print("str")
                byte_msg += msg_part.encode()

        self.lock.acquire()

        log.debug('serial sending: {}'.format(byte_msg))

        serial_return = self.ser.write(byte_msg)

        self.lock.release()


if __name__ == '__main__':
    log.basicConfig(filename='./hub-server-log.txt',
                    filemode='a',
                    format='[%(asctime)s][%(name)s][%(levelname)s][%(message)s]',
                    datefmt='%m/%d/%Y %I:%M:%S %p',
                    level=log.DEBUG)

    log.getLogger().addHandler(log.StreamHandler())
    log.info('Hub Server starting')

    hub = HMSHub()
    hub.start()
